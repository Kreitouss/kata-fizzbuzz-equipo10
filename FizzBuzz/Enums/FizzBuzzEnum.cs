﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Enums
{
    public enum FizzBuzzEnum
    {
        Fizz = 3,
        Buzz = 5,
        FizzBuzz = 15 // Divisor de 3 y 5
    }
}
