﻿using System;

namespace FizzBuzz
{
    class Program
    {
        static void Main(string[] args)
        {
            FizzBuzzCalculator fizzBuzz = new FizzBuzzCalculator();

            var numbers = fizzBuzz.calculate();

            foreach (var number in numbers)
            {
                Console.WriteLine(number);
            }
        }
    }
}
