﻿using FizzBuzz.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz
{
    public class FizzBuzzCalculator
    {
        public List<object> calculate()
        {
            List<int> numbers = new List<int>();

            for(var i = 1; i <= 100; i++)
            {
                numbers.Add(i);
            }


            List<object> result = new List<object>();
            bool intoString = false;
            foreach (var number in numbers)
            {
                foreach(FizzBuzzEnum numberEnum in Enum.GetValues(typeof(FizzBuzzEnum)).Cast<FizzBuzzEnum>().OrderByDescending(x => (int)x).ToList())
                {
                    if(number % (int)numberEnum == 0)
                    {
                        result.Add(numberEnum);
                        intoString = true;
                        break;
                    }
                }

                if (!intoString)
                {
                    result.Add(number);
                }
                intoString = false;
            }

            return result;
        }
    }
}
