using FizzBuzz;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace FizzBuzzTests
{
    public class FizzBuzzTests
    {
        [Fact]
        public void FizzBuzzNeitherFiveNorThreeMultiplesStayTheSame()
        {
            FizzBuzzCalculator fizzBuzz = new FizzBuzzCalculator();

            var numbers = fizzBuzz.calculate();

            foreach(int number in numbers.Where(z => z is int))
            {
                Assert.True(!(number % 3 == 0 && number % 5 == 0));
            }
        }

        [Fact]
        public void FizzBuzzThreeMultiplesTurnIntoFizz()
        {
            FizzBuzzCalculator fizzBuzz = new FizzBuzzCalculator();

            var numbersFizzBuzz = fizzBuzz.calculate();

            for(var i = 0; i < numbersFizzBuzz.Count(); i++)
            {
                if((i + 1) % 3 == 0 && (i + 1) % 5 != 0)
                {
                    Assert.True(numbersFizzBuzz[i].ToString() == "Fizz");
                }
                    
            }
        }

        [Fact]
        public void FizzBuzzFiveMultiplesTurnIntoBuzz()
        {
            FizzBuzzCalculator fizzBuzz = new FizzBuzzCalculator();

            var numbersFizzBuzz = fizzBuzz.calculate();

            for (var i = 0; i < numbersFizzBuzz.Count(); i++)
            {
                if ((i + 1) % 3 != 0 && (i + 1) % 5 == 0)
                {
                    Assert.True(numbersFizzBuzz[i].ToString() == "Buzz");
                }

            }
        }

        [Fact]
        public void FizzBuzzFiveAndThreeMultiplesTurnIntoFizzBuzz()
        {
            FizzBuzzCalculator fizzBuzz = new FizzBuzzCalculator();

            var numbersFizzBuzz = fizzBuzz.calculate();

            for (var i = 0; i < numbersFizzBuzz.Count(); i++)
            {
                if ((i + 1) % 3 == 0 && (i + 1) % 5 == 0)
                {
                    Assert.True(numbersFizzBuzz[i].ToString() == "FizzBuzz");
                }

            }
        }
    }
}
